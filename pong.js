var ball=new Object();
var paddle=document.getElementById("paddle");
var court=document.getElementById("court");
var strikes=document.getElementById("strikes");
var maxscore=document.getElementById("score");
var currentInterval;

function resetGame()
{
	clearInterval(currentInterval);
	setSpeed();
	ball.moving=false;
	ball.domElement.style.left='0px';
	ball.domElement.style.top=Math.round(Math.random()*400)+'px';
	ball.directionX=1;
	ball.directionY=1;
}

function startGame(event)
{
	clearInterval(currentInterval);
	var min=2*(Math.PI)/2;
	var max=3*(Math.PI)/2;
	ball.angle=((3*Math.PI)/4)+Math.random()*(max-min)+min;
	console.log("Angle: "+ball.angle);
	ball.strikes=0;
	strikes.innerHTML='0';
	ball.moving=true;
	currentInterval=setInterval(function(){playGame();}, 20/ball.speed);
	
}

function playGame()
{
	if(elementIsBeyondRightEdge(ball.domElement, court))
	{
		clearInterval(currentInterval);
		ball.moving=false;
		var currentstrikes=strikes.innerHTML;
		var currentmax=maxscore.innerHTML;
		if(currentstrikes>currentmax)
			maxscore.innerHTML=''+currentstrikes;
		strikes.innerHTML='0';
		resetGame();
	}
	else
	{
		moveBall();
		if(elementIsBeyondTopEdge(ball.domElement, court))
		{
			console.log("elementIsBeyondTopEdge");
			ball.directionY*=-1;
			moveBallVertical(10);
		}
		if(elementIsBeyondBottomEdge(ball.domElement, court))
		{
			console.log("elementIsBeyondBottomEdge");
			ball.directionY*=-1;
			moveBallVertical(-10);
		}
		if(elementIsBeyondLeftEdge(ball.domElement, court))
		{
			console.log("elementIsBeyondLeftEdge");
			ball.directionX*=-1;
			moveBallHorizontal(10);
		}
		if(ballHitPaddle())
		{
			console.log("elementIsBeyondLeftEdge");
			ball.directionX*=-1;
			ball.strikes++;
			strikes.innerHTML=ball.strikes;
			moveBallHorizontal(-10);
		}
	}
}

function moveBallVertical(amount)
{
	var currentLeft=parseInt(ball.domElement.style.left.replace('px',''));
	ball.domElement.style.left=(currentLeft+amount)+'px';
}

function moveBallHorizontal(amount)
{
	var currentLeft=parseInt(ball.domElement.style.left.replace('px',''));
	ball.domElement.style.left=(currentLeft+amount)+'px';
}


function moveBall()
{
	var yprime=ball.directionY*Math.round(Math.sin(ball.angle)*4);
	var xprime=ball.directionX*Math.round(Math.cos(ball.angle)*4);
	var currentTop=parseInt(ball.domElement.style.top.replace('px',''));
	var currentLeft=parseInt(ball.domElement.style.left.replace('px',''));
	
	console.log("currentTop: "+currentTop);
	console.log("currentLeft: "+currentLeft);
	
	ball.domElement.style.top=(currentTop+yprime)+"px";
	ball.domElement.style.left=(currentLeft+xprime)+"px";
	console.log((currentTop+yprime)+"px");
	console.log((currentLeft+xprime)+"px");
}

function getSpeed()
{
	var newspeed;
	var items=document.getElementById('speedForm').elements['speed'];
	console.log(items.length);
	for(var i=0;i<items.length;i++)
	{
		if(items[i].checked)
		{
			newspeed=items[i].value;
		}
	}
	return newspeed;
}

function setSpeed()
{
	ball.speed=getSpeed();
	console.log("Speed is set to "+ball.speed);
}

function getHeight(element)
{
	var rect=element.getBoundingClientRect();
	return Math.round(rect.bottom)-Math.round(rect.top);
}

function initialize()
{
	//This moves the paddle
	ball.domElement=document.getElementById("ball");
	document.getElementById("court").addEventListener("mousemove", function(event)
	{
		movePaddle(event);
	});
	resetGame();
}

function ballHitPaddle()
{
	var ballrect=ball.domElement.getBoundingClientRect();
	var paddlerect=paddle.getBoundingClientRect();
	if(ballrect.right>paddlerect.left && ballrect.bottom>=paddlerect.top && ballrect.top<=paddlerect.bottom) return true;
	else return false;
}

function elementIsBeyondRightEdge(elementInQuestion, refElement)
{
	var eiqBox=elementInQuestion.getBoundingClientRect();
	var refeBox=refElement.getBoundingClientRect();
	if(eiqBox.right>refeBox.right) return true;
	else return false;
}
function elementIsBeyondLeftEdge(elementInQuestion, refElement)
{
	var eiqBox=elementInQuestion.getBoundingClientRect();
	var refeBox=refElement.getBoundingClientRect();
	if(eiqBox.left<refeBox.left) return true;
	else return false;
}
function elementIsBeyondTopEdge(elementInQuestion, refElement)
{
	var eiqBox=elementInQuestion.getBoundingClientRect();
	var refeBox=refElement.getBoundingClientRect();
	if(eiqBox.top<refeBox.top) return true;
	else return false;
}
function elementIsBeyondBottomEdge(elementInQuestion, refElement)
{
	var eiqBox=elementInQuestion.getBoundingClientRect();
	var refeBox=refElement.getBoundingClientRect();
	if(eiqBox.bottom>refeBox.bottom) return true;
	else return false;
}

function movePaddle(event)
{
	//var court=document.getElementById("court");
	//var paddle=document.getElementById("paddle");
	var paddleheight=getHeight(paddle);
	var mouseY=event.clientY;
	var rect=court.getBoundingClientRect();
	var top=Math.round(rect.top);
	document.getElementById("messages").innerHTML="mouseY="+mouseY+" top="+rect.top+" inside="+(mouseY-top);
	
	if(paddleheight+(mouseY)>rect.bottom)
	{
		paddle.style.top=(rect.bottom-top-paddleheight-8)+"px";
	}
	else
	{
		paddle.style.top=(mouseY-top)+"px";
	}
}

initialize();
document.getElementById("speedForm").addEventListener("change", function() {setSpeed()});
